//
//  TableViewCell.swift
//  tableView
//
//  Created by tp on 16/02/2020.
//  Copyright © 2020 tp. All rights reserved.
//

import Foundation

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var titreCell: UILabel!
    
    
    @IBOutlet weak var descriptionCell: UILabel!
    
    @IBOutlet weak var imageCell: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

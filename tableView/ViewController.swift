//
//  ViewController.swift
//  tableView
//
//  Created by tp on 12/02/2020.
//  Copyright © 2020 tp. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    let url = URL(string: "http://eddbali.net/files/iOS/Documents/monuments.json")
    var contentData = JSON()
    var categorie = [String] ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        self.getData()
    }
    private func setTitres() -> [String]{
        let data = self.contentData
        //print(data)
        var titre = [String]()
        for elt in data["categories"].arrayValue{
            titre.append(elt["titre"].stringValue)
        }
        return titre
    }
    private func getData(){
        Alamofire.request(url!, method: .get).responseJSON
            { (response) in
                if response.result.isSuccess {
                    self.contentData = JSON(response.result.value!)
                    self.categorie = self.setTitres()
                    //print(self.categorie)
                    self.tableView.reloadData()
                }
                else
                {
                    print ("Error : \(response.result.error!)")
                }
        }
    }
    
    //plusieurs sections plusieurs lignes
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categorie.count
    }
    //la cellule ou on scroll
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellule = tableView.dequeueReusableCell(withIdentifier: "UneCellule", for : indexPath)
        
        cellule.textLabel?.text = categorie[indexPath.row]
        return cellule
    }
    
    //click sur cellule
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let Storyboard = UIStoryboard(name: "Main", bundle: nil)
        let MvC = Storyboard.instantiateViewController(withIdentifier: "MonumentsViewController") as! MonumentsViewController
        tableView.deselectRow(at: indexPath, animated: true)
    
        MvC.categorie = self.contentData["categories"][indexPath.row]
        self.navigationController?.pushViewController(MvC, animated: true)
    }
}


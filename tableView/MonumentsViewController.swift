//
//  MonumentsViewController.swift
//  tableView
//
//  Created by tp on 14/02/2020.
//  Copyright © 2020 tp. All rights reserved.
//



import UIKit
import SwiftyJSON

class MonumentsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
    /*var imageCurrent : String = ""
    var titleCurrent : String = ""
    var desriptionCurrent : String
        = ""
 */
    
    @IBOutlet weak var tableView: UITableView!
    var categorie = JSON()
    var monuments = [JSON]()
    let urlDocuments = "http://eddbali.net/files/iOS/Documents/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.monuments = categorie["monuments"].array!
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return monuments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath) as! TableViewCell
        let monument : JSON = monuments[indexPath.row]
        cell.titreCell?.text =  monument["titre"].string
        cell.descriptionCell?.text = monument["description"].string
        cell.imageCell?.image = UIImage(named: self.urlDocuments + monument["image"]["fichier"].string! )
        let url = self.urlDocuments + monument["image"]["fichier"].string!
        
        guard let imageURL = URL(string: url) else { return cell}
        // just not to cause a deadlock in UI!
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: imageURL) else { return }
            let image = UIImage(data: imageData)
            DispatchQueue.main.async {
                cell.imageCell.image = image
                self.tableView.reloadData()
            }
        }
        return cell
    }
    //click sur cellule
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    let Storyboard = UIStoryboard(name: "Main", bundle: nil)
    let DvC = Storyboard.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
    tableView.deselectRow(at: indexPath, animated: true)

    
    let monument : JSON = monuments[indexPath.row]
    DvC.titleCurrent =  monument["titre"].string!
    DvC.descriptionCurrent = monument["description"].string!
    DvC.imageCurrent = self.urlDocuments + monument["image"]["fichier"].string!
    self.navigationController?.pushViewController(DvC, animated: true)
    }
    
    // Suppression d'une cellule
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("Cellule supprimée")
            
         //   self.headlines.remove(at: indexPath.row)
            //self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
}


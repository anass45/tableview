//
//  DetailsViewController.swift
//  tableView
//
//  Created by tp on 16/02/2020.
//  Copyright © 2020 tp. All rights reserved.
//

import Foundation
import UIKit

class DetailsViewController : UIViewController {
    
    @IBOutlet weak var descriptionText: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var titre: UILabel!
    var imageCurrent : String = ""
    var titleCurrent : String = ""
    var descriptionCurrent : String = ""
    
    
    func setImage(from url: String) {
        guard let imageURL = URL(string: url) else { return }
        // just not to cause a deadlock in UI!
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: imageURL) else { return }
            let image = UIImage(data: imageData)
            DispatchQueue.main.async {
                self.img.image = image
            }
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titre.text = titleCurrent
        descriptionText.text = descriptionCurrent
        
    
        
        self.setImage(from: imageCurrent)
        
    }
}
